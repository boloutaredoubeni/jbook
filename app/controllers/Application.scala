
package controllers

// required for play
import play.api._
import play.api.mvc._

// local user defined package
import models.Item
import models.Chat

// external imports for time
import org.joda.time.LocalTime
import org.joda.time.Days
import org.joda.time.DateTime


class Application extends Controller {

  def index = Action {

    val today = DateTime.now()
    val yesterday = today.minus(Days.ONE)

    // List chats to pass to the view
    val chats = Seq(
      Chat(yesterday, 1, Seq(
        Item("me", LocalTime.now(), "Hey!"),
        Item("them", LocalTime.now(), "What?"),
        Item("me", LocalTime.now(), "Nothing")
      )),
      Chat(yesterday, 2, Seq(
        Item("me", LocalTime.now(), "Hey!"),
        Item("them", LocalTime.now(), "What?"),
        Item("me", LocalTime.now(), "Nothing")
      )),
      Chat(today, 1, Seq(
        Item("me", LocalTime.now(), "Hey!"),
        Item("them", LocalTime.now(), "What?"),
        Item("me", LocalTime.now(), "Nothing")
      )),
      Chat(today, 2, Seq(
        Item("me", LocalTime.now(), "Hey!"),
        Item("them", LocalTime.now(), "What?"),
        Item("me", LocalTime.now(), "Nothing")
      )),
      Chat(today, 3, Seq(
        Item("me", LocalTime.now(), "Hey!"),
        Item("them", LocalTime.now(), "What?"),
        Item("me", LocalTime.now(), "Nothing")
      ))
    )

    // Reply 200 OK with the chats
    Ok(views.html.index("It Works.")(chats))
  }

}
